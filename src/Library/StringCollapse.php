<?php

namespace ReliableOffshore\LaravelUtil\Library;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class StringCollapse
{
    protected static $coalesceSymbol = '^^';
    protected static $coalesceSeparator = '@@';

    /**
     * Used to collapse states so we can pass additional attributes to state
     *
     * @param  string  $state
     * @param  string  $k
     * @param  string  $v
     * @param  string  $seperator
     */
    public static function collapse(string &$state, string $k, string $v)
    {
        $assignSymbol = self::$coalesceSymbol;
        $separator    = self::$coalesceSeparator;

        if (!is_null($v))
        {
            // nullafy previous version
            $before = $k . $assignSymbol;
            if (str_contains($state, $before))
                $state = str_replace($before, $k . time() . $assignSymbol, $state);

            if (Str::contains($state, $assignSymbol)) {
                $state .= $separator;
            }

            $state .= $k . $assignSymbol . $v;
        }
    }

    /**
     * Given a state in format of collapseState this returns a value of key
     *
     * @param  string  $state
     * @param  string  $k
     * @return string
     */
    public static function expand(string $state, string $k)
    {
        $assignSymbol = self::$coalesceSymbol;
        $separator    = self::$coalesceSeparator;

        $stateParsed = [];
        $state       = str_replace([$assignSymbol, $separator], ['=', '&'], $state);
        parse_str($state, $stateParsed);
        $state = $stateParsed;

        return Arr::get($state, $k);
    }
}