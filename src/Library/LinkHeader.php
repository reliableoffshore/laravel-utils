<?php

namespace ReliableOffshore\LaravelUtil\Library;

use Illuminate\Support\Arr;

class LinkHeader
{
    public static function getNextPageLink($link)
    {
        $re   = '/<([^>]+)>; rel="next"/';
        preg_match($re, $link, $matches);

        return Arr::get($matches, 1, '');
    }
}