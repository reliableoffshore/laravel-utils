<?php

namespace ReliableOffshore\LaravelUtil\Commands;

trait CommandBeforeAfterTrait
{
    protected $class = null;

    abstract protected function process();

    // return false if you want to skip job
    protected function before()
    {
        $this->class = getClassName(get_called_class());
        $this->info("[Command {$this->class}] processing");
    }

    protected function after()
    {
        $this->info("[Command {$this->class}] processed");
    }

    public function handle()
    {
        if ($this->before() !== false)
        {
            $this->process();
            $this->after();
        }
        else
            $this->info("[Command {$this->class}] skipped");
    }
}
