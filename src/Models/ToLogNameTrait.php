<?php

namespace ReliableOffshore\LaravelUtil\Models;

trait ToLogNameTrait
{
    protected $toLogField = [];

    public function toLogName($fields = [], $includeModelName = true)
    {
        $name = null;
        $closeBracket = $hasCustomFields = false;
        $theFields = $this->toLogField;

        if (isset($this->primaryKey)) {
            $name = '#' . $this->{$this->primaryKey};
        }

        if (count($fields)) {
            $theFields = $fields;
            $hasCustomFields = true;
        } elseif (isset($this->toLogField) && is_array($this->toLogField)) {
            $theFields = $this->toLogField;
        }

        if (is_array($theFields)) {
            foreach ($theFields as $i => $field) {
                if ($hasCustomFields) {
                    $i = $i + 1;
                }

                $value = object_get($this, $field);
                $sep = ',';
                if ($i == 0) {
                    $sep = '@';
                } elseif ($i == 1) {
                    $name .= '[';
                    $sep = null;
                    $closeBracket = true;
                }

                $name .= $sep . $value;
            }
        }

        if ($closeBracket) {
            $name .= ']';
        }

        if ($includeModelName) {
            $class = getClassName(get_class($this));
            $name = $class . $name;
        }

        return $name;
    }
}
