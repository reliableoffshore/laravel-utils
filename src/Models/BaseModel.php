<?php

namespace ReliableOffshore\LaravelUtil\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

/**
 * BaseModel
 */
class BaseModel extends Model
{
    protected $toLogField = [];

    protected static function boot()
    {
        parent::boot();
    }

    public function toLogName($fields = [], $includeModelName = true)
    {
        $name = null;
        $closeBracket = $hasCustomFields = false;
        $theFields = $this->toLogField;

        if (isset($this->primaryKey)) {
            $name = '#' . $this->{$this->primaryKey};
        }

        if (count($fields)) {
            $theFields = $fields;
            $hasCustomFields = true;
        } elseif (isset($this->toLogField) && is_array($this->toLogField)) {
            $theFields = $this->toLogField;
        }

        if (is_array($theFields)) {
            foreach ($theFields as $i => $field) {
                if ($hasCustomFields) {
                    $i = $i + 1;
                }

                $value = object_get($this, $field);
                $sep = ',';
                if ($i == 0) {
                    $sep = '@';
                } elseif ($i == 1) {
                    $name .= '[';
                    $sep = null;
                    $closeBracket = true;
                }

                $name .= $sep . $value;
            }
        }

        if ($closeBracket) {
            $name .= ']';
        }

        if ($includeModelName) {
            $classNameWithNamespace = get_class($this);
            $class = mb_substr($classNameWithNamespace, strrpos($classNameWithNamespace, '\\') + 1);
            $name = $class . $name;
        }

        return $name;
    }

    /**
     * @param array|callable $attributes
     * @return static
     * @throws \Exception
     */
    public static function createIgnore($field, $value, $mixed)
    {
        $obj = static::where($field, $value)->first();

        if (!$obj) {

            // this is to bypass models $fillable
            $obj = new static();

            // callable to return an array of attribues
            if (is_callable($mixed)) {
                $obj->{$field} = $value;
                $mixed = $mixed($obj);

                if (!is_array($mixed)) {
                    throw new \Exception('@TODO - create base model exception');
                }
            }

            $mixed[$field] = $value;
            foreach ($mixed as $k => $v) {
                $obj->$k = $v;
            }

            $obj->save();
            // since we are not assigning relations and instead adding columns, we need to refresh
            $obj->refresh();
        }

        return $obj;
    }

    /**
     * @param array|callable $attributes
     * @return static
     */
    public static function upsert($field, $value, $mixed)
    {
        $obj = static::where($field, $value)->first();

        if (!$obj) {
            // this is to bypass models $fillable
            $obj = new static();
        }

        $mixed[$field] = $value;
        foreach ($mixed as $k => $v) {
            $obj->$k = $v;
        }

        $obj->save();
        // since we are not assigning relations and instead adding columns, we need to refresh
        $obj->refresh();

        return $obj;
    }

    public function get($property, $key)
    {
        return $this->$property[$key];
    }

    public function set($property, $key, $value)
    {
        $array = $this->$property;
        $array[$key] = $value;
        $this->$property = $array;

        return $this;
    }
}
