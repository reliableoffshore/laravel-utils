<?php

namespace ReliableOffshore\LaravelUtil\Models;

trait ModelMetaTrait
{
    public function get($property, $key)
    {
        if (!is_array($this->$property))
        {
            $this->$property = [];
        }

        if (array_key_exists($key, $this->$property))
        {
            return $this->$property[$key];
        }
    }

    public function set($property, $key, $value)
    {
        $array           = $this->$property;
        $array[$key]     = $value;
        $this->$property = $array;

        return $this;
    }
}
