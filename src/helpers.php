<?php

use Illuminate\Support\Facades\Log;

if (!function_exists('logd'))
{
    function logd($msg, $title = '', $context = [])
    {
        $msg = _log($msg, $title);
        Log::debug($msg, $context);
    }
}

if (!function_exists('logw'))
{
    function logw($msg, $title = '', $context = [])
    {
        $msg = _log($msg, $title);
        Log::warning($msg, $context);
    }
}

if (!function_exists('logi'))
{
    function logi($msg, $title = '', $context = [])
    {
        $msg = _log($msg, $title);
        Log::info($msg, $context);
    }
}

if (!function_exists('loge'))
{
    function loge($msg, $title = '', $context = [])
    {
        $msg = _log($msg, $title);
        Log::error($msg, $context);
    }
}

if (!function_exists('logp'))
{
    function logp($msg, $title = '', $char = '*')
    {
        $msg = _log($msg, $title);
        $repeat = str_repeat($char, 50);
        Log::info("\n" . $repeat . "\n" . $msg . "\n" . $repeat);
    }
}

if (!function_exists('_log'))
{
    function _log($x, $title = '')
    {
        if ($x instanceof \Illuminate\Database\Eloquent\Model)
            $x = $x->toArray();

        if ($title)
            $title = '['. $title .'] ';

        return $title . (is_array($x) || is_object($x) ? print_r($x, true) : $x);
    }
}

if (!function_exists('getClassName'))
{
    function getClassName($classNameWithNamespace)
    {
        return mb_substr($classNameWithNamespace, strrpos($classNameWithNamespace, '\\') + 1);
    }
}
