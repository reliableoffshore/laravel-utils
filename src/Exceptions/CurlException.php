<?php

namespace ReliableOffshore\LaravelUtil\Exceptions;

use Unirest\Response;

class CurlException extends MutableException
{
    public $response;
    public function __construct($message = "", Response $response = null, $mute = false, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
        $this->mute = true;

        $this->log($this);
    }
}
