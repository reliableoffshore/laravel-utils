<?php

namespace ReliableOffshore\LaravelUtil\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    public function render($request, Exception $exception)
    {
        if ($exception instanceof MutableException && $exception->mute)
            return;

        return parent::render($request, $exception);
    }
}
