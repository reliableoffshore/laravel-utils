<?php

namespace ReliableOffshore\LaravelUtil\Exceptions;

use Throwable;

class MutableException extends \Exception
{
    public $logged = false;
    public $mute = false;

    public function __construct($message = "", $mute = false, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->mute = true;

        $this->log($this);
    }

    public static function log(\Exception $exception, $title = '')
    {
        if (!object_get($exception, 'logged'))
        {
            $exception->logged  = true;
            $class              = get_class($exception);
            $title              = trim("$title Exception");
            $file               = '...' . substr($exception->getFile(), -20);
            $exception->_logged = true;
            loge("error: {$exception->getMessage()}, code: {$exception->getCode()}, class: $class, line: $file:{$exception->getLine()}", $title);
        }
    }
}
