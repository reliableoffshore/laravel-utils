<?php

namespace ReliableOffshore\LaravelUtil\Jobs;

trait JobBeforeAfterTrait
{
    protected $class = null;

    public function __construct()
    {
        $this->class = getClassName(get_called_class());
        logi("[Queue {$this->class}] queued");
    }

    abstract protected function process();

    // return false if you want to skip job
    protected function before()
    {
        $this->info("[Queue {$this->class}] processing");
    }

    protected function after()
    {
        $this->info("[Queue {$this->class}] processed");
    }

    public function handle()
    {
        if ($this->before() !== false)
        {
            $this->process();
            $this->after();
        }
        else
            $this->info("[Queue {$this->class}] skipped");
    }

    public function info($string)
    {
        logi($string);
    }

    public function error($string)
    {
        loge($string);
    }
}
