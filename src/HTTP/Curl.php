<?php

namespace ReliableOffshore\LaravelUtil\HTTP;

use Illuminate\Support\Arr;
use ReliableOffshore\LaravelUtil\Exceptions\CurlException;
use Unirest\Method;
use Unirest\Request;

class Curl extends Request
{
    const POST = 'POST';
    const GET = 'GET';
    const DELETE = 'DELETE';
    const PUT = 'PUT';
    private static $verifyPeer = false;

    public static function put($url, $parameters = null, $headers = [], $username = null, $password = null)
    {
        return self::send(Method::PUT, $url, $parameters, $headers, $username, $password);
    }

    public static function get($url, $parameters = null, $headers = [], $username = null, $password = null)
    {
        return self::send(Method::GET, $url, $parameters, $headers, $username, $password);
    }

    public static function post($url, $body = null, $headers = [], $username = null, $password = null)
    {
        return self::send(Method::POST, $url, $body, $headers, $username, $password);
    }

    public static function delete($url, $body = null, $headers = [], $username = null, $password = null)
    {
        return self::send(Method::DELETE, $url, $body, $headers, $username, $password);
    }

    /**
     * @param string|Method $method
     * @param string $url
     * @param null $body
     * @param array $headers
     * @param null $username
     * @param null $password
     * @param callable $errorable($response) - when the return of this !== null CurlException is thrown
     * @return \Unirest\Response
     * @throws \Unirest\Exception
     */
    public static function send($method, $url, $body = null, $headers = [], $username = null, $password = null, callable $errorable = null)
    {
        // lowercase all headers
        $method = strtoupper($method);
        $headersLowerCase = [];
        foreach ($headers as $k => $v) {
            $k = strtolower($k);
            $headersLowerCase[$k] = $v;
        }
        $headers = $headersLowerCase;

        // auto json encode
        if (is_array($body) && Arr::get($headers, 'content-type') == 'application/json') {
            $body = json_encode($body);
        }

        $response = parent::send($method, $url, $body, $headers, $username, $password);
////        logd($url);
        //        logd(['method' => $method, 'url' => $url, 'body' => $body, 'headers' => $headers, 'auth' => "$username:$password", 'response' => $response], 'Curl');

        if ($errorable)
        {
            $error = $errorable($response);
            if (!is_null($error))
            {
                throw new CurlException($response->raw_body, $response, null, $response->code);
            }
        }

        return $response;
    }
}
